var expect = require('chai').expect;
const Serviço = require('../services/Serviço');

describe('verificarRestricoesMaterial()', function(){
    it('deve verificar se o filho respeita os materiais do pai', function(){

        var matFilho= "ferro";
        var restricoes=["madeira", "MDF"];
        var resultado = true;

        var resultado2 = Serviço.verificarRestricoesMaterial(matFilho, restricoes);

        expect(resultado).to.be.equal(resultado2);
    });
});

describe('verificarCaber()', function(){
    it('deve verificar se o filho cabe dentro do pai', function(){

        var alturaFilho= 2;
        var larguraFilho= 6;
        var profundidadeFilho = 8;
        var alturaPai = 5;
        var larguraPai = 10;
        var profundidadePai = 15;
        var resultado = true;

        var resultado2 = Serviço.verificarCaber(alturaFilho, larguraFilho, profundidadeFilho,
            alturaPai, larguraPai, profundidadePai);

        expect(resultado).to.be.equal(resultado2);
    });
});

describe('verificarRestricoesOcupacao()', function(){
    it('deve verificar se o filho cabe nos limites do pai', function(){
        
        var alturaFilho= 6;
        var larguraFilho= 2;
        var profundidadeFilho = 8;
        var alturaPai = 5;
        var larguraPai = 10;
        var profundidadePai = 15;
        var restricoes = [10,50];
        var resultado = true;

        var resultado2 = Serviço.verificarRestricoesOcupacao(alturaFilho, larguraFilho, profundidadeFilho,
            alturaPai, larguraPai, profundidadePai, restricoes);

        expect(resultado).to.be.equal(resultado2);
    });
});

describe('calcularVolume()', function(){
    it('deve calcular o volume', function(){

        var altura= 2;
        var largura= 2;
        var profundidade = 2;
        var resultado = altura * largura * profundidade;

        var resultado2 = Serviço.calcularVolume(altura, largura, profundidade);
        console.log(resultado2);
        expect(resultado).to.be.equal(resultado2);
    });
});

describe('aumentarPreçoEncomenda()', function(){
    it('deve aumentar o preço', function(){

        var preçoTotal=0;
        var preço=5;
        var resultado = preçoTotal + preço;

        Serviço.aumentarPreçoEncomenda(preçoTotal, preço);

        expect(resultado).to.be.equal(preçoTotal+preço);
    });
});