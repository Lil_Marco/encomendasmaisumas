
var rootCas = require('ssl-root-cas/latest').create();
require('https').globalAgent.options.ca = rootCas;
const express = require('express');
const bodyParser = require('body-parser');
// initialize our express app
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const encomenda = require('./routes/EncomendaRoute'); // Imports routes 
const itemDeProduto = require('./routes/ItemDeProdutoRoute');
const userRoutes = require('./routes/UserRoutes');
const cidadesRoutes = require('./routes/CidadeRoute');
const fabricasRoutes = require('./routes/FabricaRoute');

const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://Lil_Marco:abcd1234@ds145053.mlab.com:45053/sicencomendas';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var cors = require('cors');
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));



app.use('/encomendas', encomenda);
app.use('/itensDeProduto', itemDeProduto);
app.use('/users', userRoutes);
app.use('/cidades', cidadesRoutes);
app.use('/fabricas', fabricasRoutes);




let port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
    



    const datatime = require('./services/DataTimeService');


    console.log(datatime.getTime().toString());

    datatime.remove();
});
