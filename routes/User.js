const express = require('express');
const router = express.Router();

// Require the controllers
const user_controller = require('../controllers/UserController');

router.post('/', user_controller.user_new);

router.get('/', user_controller.user_getAll);

router.get('/:id', user_controller.user_getByID);

router.put('/:id', user_controller.user_update);

router.delete('/:id', user_controller.user_delete);


module.exports = router;