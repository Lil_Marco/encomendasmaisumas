const express = require('express');
const router = express.Router();

// Require the controllers
const cidade_controller = require('../controllers/CidadeController');

router.post('/', cidade_controller.post);

router.get('/', cidade_controller.getAll);

router.get('/nome/:nomeCidade', cidade_controller.getCidadeByNome);

router.get('/:id', cidade_controller.getCidadeByID);

module.exports = router;