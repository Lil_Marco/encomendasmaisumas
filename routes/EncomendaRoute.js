const express = require('express');
const router = express.Router();

// Require the controllers
const encomenda_controller = require('../controllers/EncomendaController');

//POST
router.post('/', encomenda_controller.encomenda_create);
//GETALL
router.get('/', encomenda_controller.encomenda_getAll);
//GETBYID
router.get('/:id', encomenda_controller.encomenda_getByID);

router.get('/user/:idUser', encomenda_controller.encomenda_getByUser);

router.get('/estado/:estado', encomenda_controller.encomenda_getByEstado);

router.get('/:id/Itens', encomenda_controller.encomenda_getItensDeEncomendaByID);

router.get('/:idEnc/Itens/:idItem', encomenda_controller.encomenda_getItemByIDDeEncomendaByID);
//UPDATE
router.put('/:id', encomenda_controller.encomenda_update);

router.put('/estadoUser/:id', encomenda_controller.encomenda_updateComEstadoUser);

router.put('/semEstadoUser/:id', encomenda_controller.encomenda_updateUser);
//DELETE
router.delete('/:id', encomenda_controller.encomenda_delete);

module.exports = router;






module.exports = router;