const express = require('express');
const router = express.Router();

// Require the controllers
const fabrica_controller = require('../controllers/FabricaController');

router.post('/', fabrica_controller.post);

router.get('/', fabrica_controller.getAll);

router.get('/:id', fabrica_controller.getFabricaByID);

router.put('/:id', fabrica_controller.updateFabrica);

router.post('/caminho', fabrica_controller.getCaminho);

router.delete('/:id', fabrica_controller.deleteFabrica);

module.exports = router;