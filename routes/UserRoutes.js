const express = require('express');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var User = require('../models/User');
var VerifyToken = require('../Middleware/VerifyToken');
var nodemailer = require('nodemailer');

const router = express.Router();

// Require the controllers
const user_controller = require('../controllers/UserController');

router.post('/register',  user_controller.user_new);

router.post('/login', function(req, res){

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'sic.producoes2018@gmail.com',
               pass: 'arqsi_lapr'
           }
       });

    let encontrou = false;

    User.find(function (err, item) {
        if (err) return next(err);
        item.forEach(element => {

            if( element.email == req.body.email && encontrou == false){
                encontrou = true;
                console.log(element);

                if(!bcrypt.compareSync(req.body.password, element.password )){
                return  res.status(401).send({auth:false,token:null,message: 'Authfailed.'});
                }else{


                   
                    const payload= {user:element.email};
                    var theToken = jwt.sign(payload, 'TheSecret_123456789', {expiresIn:86400});



                    const mailOptions = {
                        from: 'sic.producoes2018@gmail.com', // sender address
                        to: element.email, // list of receivers
                        subject: 'Subject of your email', // Subject line
                        html: theToken// plain text body
                      };


                      transporter.sendMail(mailOptions, function (err, info) {
                        if(err)
                          console.log(err)
                        else
                          console.log(info);
                     });


                     
                    res.json({success:true,message:'Enjoy your token!',token:theToken, id : element.id});

        

                }
                      
            }
        });


        if(encontrou == false){
            res.json({success:false, message:"Auth Fail"});
        }
    })


    /*
    User.findOne({email: req.body.email}, function(err, user){

        if(err) res.send("ERRO: Email invalido");
        if(!user) {res.json({success:false, message:"Auth Fail"});}
        else if(user){
            if(!bcrypt.compareSync(req.body.password, user.password ))
                return  res.status(401).send({auth:false,token:null,message: 'Authfailed.'});
        }else{

            const payload= {user:user.email};
            var theToken = jwt.sign(payload, 'TheSecret_123456789', {expiresIn:86400});
            res.json({success:true,message:'Enjoy your token!',token:theToken});

        }
    });
    */

});


router.get('/', VerifyToken, user_controller.user_getAll);

router.get('/:id', user_controller.user_getByID);

router.put('/:id', user_controller.user_update);

router.delete('/:id', user_controller.user_delete);


module.exports = router;