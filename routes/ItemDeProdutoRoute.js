

const express = require('express');
const router = express.Router();

// Require the controllers
const itemDeProduto_controller = require('../controllers/ItemDeProdutoController');

router.post('/', itemDeProduto_controller.post);

router.post('/create', itemDeProduto_controller.item_create);

router.post('/create2', itemDeProduto_controller.item_create2);

router.get('/', itemDeProduto_controller.getAll);

router.get('/:id', itemDeProduto_controller.getItemDeProdutoByID);

router.put('/:id', itemDeProduto_controller.updateItemDeProduto);

router.delete('/:id', itemDeProduto_controller.deleteItemDeProduto);


module.exports = router;