const Encomenda = require('../models/Encomenda');
const Item = require('../models/ItemDeProduto');

class Serviço {

    preencherItemPai(itemDeProduto, data) {
        itemDeProduto.nome = data.nome;
        itemDeProduto.preço = data.preço;
        itemDeProduto.categoria = parseInt(data.categoria);
      //  itemDeProduto.cor = data.cores[0];
      console.log("A TUA MAE");
      console.log(itemDeProduto);
    }

    verificarMaterial(itemDeProduto, data, materialBody) {
        if (data.materiais.includes(materialBody)) {
            itemDeProduto.material = materialBody;
            return true;
        }
    }

    verificarDimensoes(itemDeProduto, data, alturaBody, larguraBody, profundidadeBody) {
        if (data.altura.length == 2) {
            if (parseInt(data.altura[0]) <= parseInt(alturaBody) && parseInt(alturaBody) <= parseInt(data.altura[1])) {
                itemDeProduto.dimensao.altura = parseInt(alturaBody);
            } else {
                return false;
            }
        } else {
            if (parseInt(data.altura[0]) == parseInt(alturaBody)) {
                itemDeProduto.dimensao.altura = parseInt(alturaBody);
            } else {
                return false;
            }
        }

        if (data.largura.length == 2) {
            if (parseInt(data.largura[0]) <= parseInt(larguraBody) && parseInt(larguraBody) <= parseInt(data.largura[1])) {
                itemDeProduto.dimensao.largura = parseInt(larguraBody);
            } else {
                return false;
            }
        } else {
            if (parseInt(data.largura[0]) == larguraBody) {
                itemDeProduto.dimensao.largura = parseInt(larguraBody);
            } else {
                return false;
            }
        }

        if (data.profundidade.length == 2) {
            if (parseInt(data.profundidade[0]) <= parseInt(profundidadeBody) && parseInt(profundidadeBody) <= parseInt(data.profundidade[1])) {
                itemDeProduto.dimensao.profundidade = parseInt(profundidadeBody);
            } else {
                return false;
            }
        } else {
            if (parseInt(data.profundidade[0]) == profundidadeBody) {
                itemDeProduto.dimensao.profundidade = parseInt(profundidadeBody);
            } else {
                return false;
            }
        }
        return true;
    }

    preencherItemFilho(itemDeProdutoFilho, dataFilho) {
        itemDeProdutoFilho.nome = dataFilho.nome;
        itemDeProdutoFilho.preço = dataFilho.preço;
        itemDeProdutoFilho.categoria = parseInt(dataFilho.categoria);
        itemDeProdutoFilho.material = dataFilho.materiais[0];
        itemDeProdutoFilho.dimensao.altura = parseInt(dataFilho.altura[0]);
        itemDeProdutoFilho.dimensao.largura = parseInt(dataFilho.largura[0]);
        itemDeProdutoFilho.dimensao.profundidade = parseInt(dataFilho.profundidade[0]);
        itemDeProdutoFilho.cor = dataFilho.cores[0];
    }

    verificarRestricoesMaterial(materiaisFilho, restricoes) {

        for (var i = 0; i < materiaisFilho.length; i++) {
            if (restricoes.includes(materiaisFilho[i])) {
                return false;
            }
        }
        return true;
    }

    verificarCaber(alturaFilho, larguraFilho, profundidadeFilho,
        alturaPai, larguraPai, profundidadePai) {

        if (alturaFilho > alturaPai) {
            return false;
        }
        if (larguraFilho > larguraPai) {
            return false;
        }
        if (profundidadeFilho > profundidadePai) {
            return false;
        }
        return true;
    }

    verificarRestricoesOcupacao(alturaFilho, larguraFilho, profundidadeFilho,
        alturaPai, larguraPai, profundidadePai, restricoes) {

        var limMin = restricoes[0];
        var limMax = restricoes[1];

        var Vpai = this.calcularVolume(alturaPai, larguraPai, profundidadePai);
        var Vfilho = this.calcularVolume(alturaFilho, larguraFilho, profundidadeFilho);

        if (Vpai * (limMin * 0.01) > Vfilho) {
            return false;
        }

        if (Vpai * (limMax * 0.01) < Vfilho) {
            return false;
        }
        return true;
    }

    calcularVolume(altura, largura, profundidade) {
        return altura * largura * profundidade;
    }

    aumentarPreçoEncomenda(preçoTotal, preço) {
        preçoTotal = preçoTotal + preço;
    }

}

module.exports = new Serviço();