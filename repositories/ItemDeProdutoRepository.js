const Item = require('../models/ItemDeProduto');
const ItemDTO = require('../DTOs/ItemDeProdutoDTO')

class ItemDeProdutoRepository{

    saveItem(item, req, res){
        item.nome = req.body.nome;
        item.preço = req.body.preço;
        item.categoria = req.body.categoria;
        item.material = req.body.material;
        item.acabamento = req.body.acabamento;
        item.dimensao.altura = req.body.altura;
        item.dimensao.largura = req.body.largura;
        item.dimensao.profundidade = req.body.profundidade;
        item.itensFilho = req.body.itensFilho;
        item.produto = req.body.produto;
        item.cor = req.body.cor;

        item.save(function (err) {
            console.log("erro->->"+err);
            if(err){
                return next(err);
            }
            res.send(ItemDTO.ItemDeProdutoDTO(item));
        })
    }
    saveItem2(item, encomenda, req, res){

        console.log(JSON.stringify(item));

        item.nome = req.body.nome;
        item.preço = req.body.preço;
        item.categoria = req.body.categoria;
        item.material = req.body.material;
        item.acabamento = req.body.acabamento;
        item.dimensao.altura = req.body.altura;
        item.dimensao.largura = req.body.largura;
        item.dimensao.profundidade = req.body.profundidade;
        item.itensFilho = req.body.itensFilho;
        item.produto = req.body.produto;
        item.cor = req.body.cor;

        item.save(function (err) {
            if(err){
                console.log("hey" + err)
                return next(err);
            }
            encomenda.itens.push(item);
            encomenda.save(function (err) {
                if(err){
                    return next(err);
                }
                console.log("Encomenda atualizada com o novo item.")
            });

            res.send(ItemDTO.ItemDeProdutoDTO(item));
        })
    }

    saveItemPai(itemDeProduto, encomenda, preçoTotal, res){
        itemDeProduto.save(function (err) {
            if(err){
                return next(err);
            }
            console.log(preçoTotal);
            encomenda.itens.push(itemDeProduto.id);

            encomenda.save(function (err) {
                if(err){
                    return next(err);
                }
                console.log("Encomenda atualizada com o novo item.")
            });

            res.send(ItemDTO.ItemDeProdutoDTO(itemDeProduto));
        });
    }

    saveItemFilho(itemDeProdutoFilho){
        itemDeProdutoFilho.save(function (err) {
            if(err){
                return next(err);
            }
        });
    }

    getAllItens(req, res){
        Item.find(function (err, item) {
            if (err) return next(err);
            res.send(item);
        })
    }

    getItemDeProdutoByID(req, res){
        Item.findById(req.params.id, function (err, item) {
            if (err) return next(err);
            res.send(ItemDTO.ItemDeProdutoDTO(item));
        })
    }

    updateItemDeProduto(req, res){
        let itemProd;
        const promise = this.getItem(req.params.id, itemProd);

        promise.then(() => { 
            Item.findByIdAndUpdate(req.params.id, {$set: req.body},
                function (err, item) {
                    if (err) return next(err);
                    res.send(itemProd);
            });
        })
        .catch( function (err){
           console.log("Erro update da Encomenda");
        });
    }

    deleteItemDeProduto(req, res){
        let itemProd;
        const promise = this.getItem(req.params.id, itemProd);

        promise.then(() => { 
            Item.findByIdAndRemove(req.params.id, function (err) {
                if (err) return next(err);
                res.send(itemProd);
            })
        })
        .catch( function (err){
           console.log("Erro Delete da Encomenda");
        });
    }

    getItem(id, itemProd){
        return new Promise((resolve, reject) => {    
             Item.findById(id, function (err, item) {
                 if (err) return next(err);
                itemProd = item;
            })   
        resolve(itemProd);
        })
    }
}

module.exports = new ItemDeProdutoRepository();