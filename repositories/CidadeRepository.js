const Cidade = require('../models/Cidade');
const http = require('http');

var Client = require('node-rest-client').Client;
var client = new Client();

class CidadeRepository{

    post(cidade, req, res){
        cidade.nome = req.body.nome;

        var url = 'http://api.opencagedata.com/geocode/v1/json?q=PLACENAME&key=dc5f63f2926b43f0bca8bfcdf3dab3cc';
        let urlFinal = url.replace('PLACENAME', req.body.nome);
        //console.log(urlFinal);
        const prom = new Promise((resolve, reject) => {
            //http.request(urlFinal, function(response){
            client.get(urlFinal, function (data, response) {
                console.log(data);
                if(data.results.length == 0 || data.results == undefined){
                    res.send("Cidade Inválida");
                }else{
                    cidade.latitude = data.results[0].geometry.lat;
                    cidade.longitude = data.results[0].geometry.lng;
                }

                resolve(cidade);
            }).on('error', function(e) {
                res.sendStatus(500);
            }).end()
        });

        Promise.resolve(prom)
            .then(() => {
                cidade.save(function (err) {
                    if(err){
                        res.json({message: err});
                    }
                   
                    res.json(cidade);
                })
            })
            .catch(function (err) {
                console.log(err);
                return (err);
            });

    }

    getAllCidades(req, res){
        Cidade.find(function (err, cidade) {
            if (err) res.send(err);
            res.send(cidade);
        })
    }

    getCidadeByNome(req, res){
        Cidade.findOne({'nome': req.params.nomeCidade}, function (err, cidade) {
            if (err) res.send(err);
            res.send(cidade);
        })
    }

    getCidadeByID(req, res){
        Cidade.findById(req.params.id, function (err, cidade) {
            if (err) res.send(err);
            res.json(cidade);
        })
    }

}

module.exports = new CidadeRepository();