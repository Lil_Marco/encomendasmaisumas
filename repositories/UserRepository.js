const User = require('../models/User');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
//var VerifyToken = require('../auth/VerifyToken');



const ItemDTO = require('../DTOs/ItemDeProdutoDTO')

class UserRepository{



    saveUser(user, req, res){

        user.nome = req.body.nome;
        user.morada = req.body.morada;
        user.cidade = req.body.cidade;
        user.codigoPostal = req.body.codigoPostal;
        user.nCC = req.body.nCC;
        user.mExp = req.body.mExp;
        user.yExp = req.body.yExp;
        user.cVV = req.body.cVV;
        user.email = req.body.email;
        user.firstLogin = req.body.firstLogin;
        user.password = bcrypt.hashSync(req.body.password, 8);
        

     
        user.save(function (err) {
            if(err){
               res.send("ERROR: Invalid User data");
            }


            res.json({message: "User created!", _id: user._id.toString() });
        })

        
    }

    


    getAllUsers(req, res){

        User.find(function (err, item) {
            if (err) return next(err);
            res.send(item);
        })
    }

    getUserByID(req, res){
        User.findById(req.params.id, function (err, user) {
            if (err) res.send("ERROR: Invalid id");
            res.send(user);
        })
    }

    updateUser(req, res){
        let itemProd;
        const promise = this.getUser(req.params.id, itemProd);

        promise.then(() => { 
            User.findByIdAndUpdate(req.params.id, {$set: req.body},
                function (err, item) {
                    if (err) return next(err);
                    res.send(itemProd);
            });
        })
        .catch( function (err){
           console.log("Erro update da Encomenda");
        });
    }

    deleteUser(req, res){
        let itemProd;
        const promise = this.getUser(req.params.id, itemProd);

        promise.then(() => { 
            User.findByIdAndRemove(req.params.id, function (err) {
                if (err) res.send("ERROR: Invalid Delete");
                res.send(itemProd);
            })
        })
        .catch( function (err){
          res.send("ERROR: Invalid delete");
        });
    }

    getUser(id, user){
        return new Promise((resolve, reject) => {    
             User.findById(id, function (err, item) {
                 if (err) return next(err);
                user = item;
            })   
        resolve(user);
        })
    }
}

module.exports = new UserRepository();