const Encomenda = require('../models/Encomenda');
const { Estados } = require('../models/Encomenda');
const Item = require('../models/ItemDeProduto');
const EncomendaDTO = require('../DTOs/EncomendaDTO')
const ItemDTO = require('../DTOs/ItemDeProdutoDTO')

class EncomendaRepository{

    saveEncomenda(encomenda, req, res){
        encomenda.identificador = req.body.identificador;
        encomenda.preço = 0;
        encomenda.estado = Estados.Criada;
        encomenda.cidade = req.body.cidade

        encomenda.save(function (err) {
            if(err){
                return next(err);
            }
           
            res.send(EncomendaDTO.encomendaDTO(encomenda));
        })
    }

    getAllEncomendas(req, res){
        Encomenda.find(function (err, encomenda) {
            if (err) return next(err);
            res.send(encomenda);
        })
    }

    getEncomendaByID(req, res){
        Encomenda.findById(req.params.id, function (err, encomenda) {
            if (err) return next(err);
            res.send(EncomendaDTO.encomendaDTO(encomenda));
        })
    }

    

    getItensDeEncomendaByID(req, res){

        Encomenda.findById(req.params.id).populate({
            path : 'itens'    
        }).exec(function (err, encomenda) {
            if (err) return next(err);
            res.send(encomenda.itens);
        })
    }

    getItemByIDDeEncomendaByID(req, res){
        Encomenda.findById(req.params.idEnc, function (err, encomenda) {
            var flag=false;
            if (err) return next(err);
            for(var i=0; i < encomenda.itens.length; i++){
                if(encomenda.itens[i]==req.params.idItem){
                    flag = true;      
                }
            }
            if(flag){
                Item.findById(req.params.idItem, function(err, item){
                    if(err) return next(err)
                    res.send(ItemDTO.ItemDeProdutoDTO(item));
                })
            }else{
                res.status(404).json({Resultado: 'Não existe esse item nesta encomenda!'})
            }
        })
    }

    updateEncomenda(req, res){
        Encomenda.findByIdAndUpdate(req.params.id, {$set: req.body},
            function (err, encomenda) {
                if (err) return next(err)
                else{
                    if(req.body.estado == 2){
                        encomenda.estado = Estados.Validada
                        encomenda.save(function (err) {
                            if(err){
                                return next(err);
                            } 
                            res.send(EncomendaDTO.encomendaDTO(encomenda));
                        })
                    }
                    else if(req.body.estado == 3){
                        encomenda.estado = Estados.Assignada
                        encomenda.save(function (err) {
                            if(err){
                                return next(err);
                            } 
                            res.send(EncomendaDTO.encomendaDTO(encomenda));
                        })
                    }
                    else if(req.body.estado == 4){
                        encomenda.estado = Estados.ProntaExpedir
                        encomenda.save(function (err) {
                            if(err){
                                return next(err);
                            } 
                            res.send(EncomendaDTO.encomendaDTO(encomenda));
                        })
                    }
                    else if(req.body.estado == 5){
                        encomenda.estado = Estados.Entregue
                        encomenda.save(function (err) {
                            if(err){
                                return next(err);
                            } 
                            res.send(EncomendaDTO.encomendaDTO(encomenda));
                        })
                    }
                    else{
                        res.send(EncomendaDTO.encomendaDTO(encomenda));
                    }
                }
        });
    }

    updateEncomendaComEstadoUser(req, res){
        Encomenda.findByIdAndUpdate(req.params.id, {$set: req.body},
            function (err, encomenda) {
                if (err) return next(err)
                else{
                    encomenda.user = req.body.user;
                    if(req.body.estado == 2){
                        encomenda.preço = req.body.preço;
                        encomenda.estado = Estados.Validada
                        encomenda.save(function (err) {
                            if(err){
                                return next(err);
                            } 
                            res.send(EncomendaDTO.encomendaDTO(encomenda));
                        })
                    }
                    else if(req.body.estado == 3){
                        encomenda.preço = req.body.preço;
                        encomenda.estado = Estados.Assignada
                        encomenda.save(function (err) {
                            if(err){
                                return next(err);
                            } 
                            res.send(EncomendaDTO.encomendaDTO(encomenda));
                        })
                    }
                    else if(req.body.estado == 4){
                        encomenda.preço = req.body.preço;
                        encomenda.estado = Estados.ProntaExpedir
                        encomenda.save(function (err) {
                            if(err){
                                return next(err);
                            } 
                            res.send(EncomendaDTO.encomendaDTO(encomenda));
                        })
                    }
                    else if(req.body.estado == 5){
                        encomenda.preço = req.body.preço;
                        encomenda.estado = Estados.Entregue
                        encomenda.save(function (err) {
                            if(err){
                                return next(err);
                            } 
                            res.send(EncomendaDTO.encomendaDTO(encomenda));
                        })
                    }
                    else{
                        res.send(EncomendaDTO.encomendaDTO(encomenda));
                    }
                }
        });
    }
    updateEncomendaUser(req, res){
        Encomenda.findByIdAndUpdate(req.params.id, {$set: req.body},
            function (err, encomenda) {
            if (err) return next(err)
            else{
                encomenda.user = req.body.user;
                res.send(EncomendaDTO.encomendaDTO(encomenda));
            }
        });
    }

    deleteEncomenda(req, res){

        const promise = this.deleteItensDaEncomenda(req.params.id);

        promise.then(() => {
            
            Encomenda.findByIdAndRemove(req.params.id, function (err) {
                if (err) return next(err);
                res.send('Deleted Successfully!');
            })
        })
        .catch( function (err){
           console.log("Erro Delete da Encomenda");
        });  
    }

    deleteItensDaEncomenda(id){
        return new Promise((resolve, reject) => {    
            Encomenda.findById(id, function(err, encomenda) {
                if (err) reject("Erro");
                else{
                    if(encomenda != null){
                        encomenda.itens.forEach(idItem => {
                            Item.findByIdAndDelete(idItem, function(err) {
                                if(err) return next(err);
                            })
                        })
                    }
                }
                resolve(id);
            })
        })
    }

    getEncomendasByUser(req, res){
        var encomendas = [];
        const promise = this.getEncomendasUser(req.params.idUser, encomendas);

        promise.then(() => {

            res.send(encomendas);
        })
        .catch( function (err){
           console.log("Erro get by user");
        }); 

    }
    getEncomendasUser(id, encomendas){
        return new Promise((resolve, reject) => {   
            
            Encomenda.find(function (err, encomenda) {
                if (err) reject("Erro");
                else{
                    for(var i=0; i < encomenda.length; i++){
                        if(encomenda[i].user==id){
                            encomendas.push(encomenda[i]);
                        }
                    }
                }
                resolve(encomendas);
            })
        })
    }

    getEncomendasByEstado(req, res){
        var encomendas = [];
        const promise = this.getEncomendasEstado(req.params.estado, encomendas);

        promise.then(() => {

            res.send(encomendas);
        })
        .catch( function (err){
           console.log("Erro get by estado");
        }); 
    }
    getEncomendasEstado(estado, encomendas){
        return new Promise((resolve, reject) => {   
            
            Encomenda.find(function (err, encomenda) {
                if (err) reject("Erro");
                else{
                    for(var i=0; i < encomenda.length; i++){
                        if(encomenda[i].estado==estado){
                            encomendas.push(encomenda[i]);
                        }
                    }
                }
                resolve(encomendas);
            })
        })
    }
    //----------------------------MÉTODOS AUXILIARES----------------------------

    getEncomenda(id){
        Encomenda.findById(id, function (err, encomenda) {
            if (err) return next(err);
            return encomenda;
        })
    }


}

module.exports = new EncomendaRepository();