const Cidade = require('../models/Cidade');
const Fabrica = require('../models/Fabrica');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var Client = require('node-rest-client').Client;
var client = new Client();

class FabricaRepository{

    post(fabrica, req, res){
        fabrica.nome = req.body.nome;
        
        Cidade.findById(req.body.idCidade, function (err, cidade) {
            if (err) res.send(err);
            fabrica.local = req.body.idCidade;
            
            fabrica.save(function (err) {
                if(err){
                    res.json({message: err});
                }
               
                res.json(fabrica);
            })
        })
    }

    getAllFabricas(req, res){
        Fabrica.find(function (err, fabrica) {
            if (err) res.send(err);
            res.send(fabrica);
        })
    }

    getFabricaByID(req, res){
        Fabrica.findById(req.params.id, function (err, fabrica) {
            if (err) res.send(err);
            res.json(fabrica);
        })
    }

    updateFabrica(req, res){
        console.log("LUL: " + req.params.id);
        Fabrica.findByIdAndUpdate(req.params.id, {$set: req.body}, 
            function (err, fabrica) {
            if (err) res.send(err);
            
            //fabrica.encomendas = req.body.encomendas;
            let x = req.body.encomendas.split(',');

            console.log("X: " + x.length);
            let encs = [];
            for(var i =0 ; i < x.length; i++){
                encs.push(x[i]);
            }
            fabrica.encomendas = encs;
            fabrica.save(function (err) {
                if(err){
                    res.json({message: err});
                }
               
                res.json(fabrica);
            })   
        });
    
    }

    deleteFabrica(req, res){
        let fabrica;
        const promise = this.getFabrica(req.params.id, fabrica);

        promise.then(() => { 
            Fabrica.findByIdAndRemove(req.params.id, function (err) {
                if (err) return next(err);
                res.send(fabrica);
            })
        })
        .catch( function (err){
           console.log("Erro Delete da Fabrica");
        });
    }
    getFabrica(id, fabrica){
        return new Promise((resolve, reject) => {    
            Fabrica.findById(id, function (err, item) {
                 if (err) return next(err);
                 fabrica = item;
            })   
        resolve(fabrica);
        })
    }


    getCaminho(req, res){

        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log("ola");

                    res.send(this.responseText);
            }else{
                console.log("adeus");

                console.log(this.responseText);

            }
          };
          xhttp.open("POST", "http://40.89.155.139:5000/encomenda", true);
          xhttp.setRequestHeader("Content-Type", "application/json");
          console.log(JSON.stringify(req.body));
          xhttp.send(JSON.stringify(req.body));


        /*

        var ajax = new XMLHttpRequest();
                    ajax.onreadystatechange = function () {
                        if (ajax.readyState == 4 && ajax.status == 200) {
                            var caminhoEncomenda = ajax.responseText;
                            
                            console.log(caminhoEncomenda);
                            ajax.send(caminhoEncomenda);
                            res.send(caminhoEncomenda);
                            // res.send(fabrica);
                        } else if (ajax.readyState == 4 && ajax.status == 400) {
                            //alert(ajax.responseText);
                            console.log(ajax.responseText);

                        }
                    }
                    ajax.open("POST", "http://40.89.155.139:5000/encomenda");
                    ajax.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    ajax.send(null);

                    */
        //client.post(urlFinal, function (data, response) {
        
    }

}

module.exports = new FabricaRepository();