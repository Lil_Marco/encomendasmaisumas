const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CidadeSchema = new Schema({
    nome: {type: String},
    latitude: {type: Number},
    longitude: {type: Number}
});


// Export the model
module.exports = mongoose.model('Cidade', CidadeSchema);