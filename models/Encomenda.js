const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Estados = Object.freeze({Criada:'criada', Validada:'validada', Assignada:'assignada',
                               ProntaExpedir:'pronta a expedir', Entregue:'entregue'});

let EncomendaSchema = new Schema({
    identificador: {type: String, required: true, max: 10},
    preço: {type: Number, required: true},
    data: {type: Date, default: Date.now},
    estado: {type: String, enum: Object.values(Estados)},
    itens: [{type: mongoose.Schema.Types.Object, ref:'ItemDeProduto'}],
    user: {type: mongoose.Schema.Types.ObjectId, ref:'User'},
    cidade: {type: String}
});

Object.assign(EncomendaSchema.statics, {
    Estados,
});

// Export the model
module.exports = mongoose.model('Encomenda', EncomendaSchema);