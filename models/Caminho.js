const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CaminhoSchema = new Schema({
    caminho: {type: String},
    distancia: {type: Number}
});


// Export the model
module.exports = mongoose.model('Caminho', CaminhoSchema);