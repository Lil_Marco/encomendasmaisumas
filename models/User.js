var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;


var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

var UserSchema = new Schema({
nome: {type: String, unique: true },
morada: String,
cidade: String,
codigoPostal: String,
nCC : String,
mExp : String,
yExp : String,
cVV : String,
firstLogin : { type: String, default: Date.now() },
email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: 'Email address is required',
    validate: [validateEmail, 'Please fill a valid email address'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
},
password : String
});

/* RETIADO DE: https://stackoverflow.com/a/24214767 */

module.exports = mongoose.model('User', UserSchema);