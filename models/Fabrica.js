const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FabricaSchema = new Schema({
    nome: {type: String},
    local: {type: mongoose.Schema.Types.ObjectId, ref:'Cidade'},
    encomendas: [String]
});


// Export the model
module.exports = mongoose.model('Fabrica', FabricaSchema);