const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ItemDeProdutoSchema = new Schema({

    nome: { type: String, required: true, max: 100 },
    preço: { type: Number, required: true },
    categoria: Number,
    material: String,
    acabamento: String,
    dimensao: {
        altura: Number,
        largura: Number,
        profundidade: Number
    },
    itensFilho: [this],
    produto: Number,
    cor: String
});
// Export the model
module.exports = mongoose.model('ItemDeProduto', ItemDeProdutoSchema);