const User = require('../models/User');
const repository = require('../repositories/UserRepository');
const dataService = require('../services/DataTimeService');
var nodemailer = require('nodemailer');

//POST
exports.user_new = function (req, res) {
    var user = new User();


    
  
    console.log("OLA");

       const theToken = "Vamos guardar os seus dados por " + dataService.getTime().toString();

       console.log("THE TOKEN:" + theToken);
       console.log("The EMAIL:" + req.body.email);

       
       

       var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'sic.producoes2018@gmail.com',
               pass: 'arqsi_lapr'
           }
       });
       const mailOptions = {
        from: 'sic.producoes2018@gmail.com', // sender address
        to: req.body.email, // list of receivers
        subject: 'Welcome to SiC-Productions. ', // Subject line
        html: theToken// plain text body
      };


      transporter.sendMail(mailOptions, function (err, info) {
        if(err)
          console.log(err)
        else
          console.log(info);
     });

     


    repository.saveUser(user, req, res);

    
};

//GET ALL
exports.user_getAll = function (req, res) {
    repository.getAllUsers(req, res);
}

//GET BY ID
exports.user_getByID = function (req, res) {
    repository.getUserByID(req, res);
};

//PUT
exports.user_update = function (req, res) {
    repository.updateUser(req, res);
};

//DELETE
exports.user_delete = function (req, res) {
    repository.deleteUser(req, res);
};