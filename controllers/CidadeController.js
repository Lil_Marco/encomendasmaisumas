const Cidade = require('../models/Cidade');
const repository = require('../repositories/CidadeRepository');

//POST
exports.post = function (req, res) {
    let cidade = new Cidade();

    repository.post(cidade, req, res);
};

//GET ALL
exports.getAll = function (req, res) {
    repository.getAllCidades(req, res);
}

//GET BY ID
exports.getCidadeByNome = function (req, res) {
    repository.getCidadeByNome(req, res);
};

exports.getCidadeByID = function (req, res){
    repository.getCidadeByID(req, res);
};