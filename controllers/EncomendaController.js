const Encomenda = require('../models/Encomenda');
const repository = require('../repositories/EncomendaRepository');

//POST
exports.encomenda_create = function (req, res) {
    var encomenda = new Encomenda();

    repository.saveEncomenda(encomenda, req, res);
};

//GET ALL
exports.encomenda_getAll = function (req, res) {
    repository.getAllEncomendas(req, res);
}

//GET BY ID
exports.encomenda_getByID = function (req, res) {
    repository.getEncomendaByID(req, res);
};

exports.encomenda_getByUser = function (req, res) {
    repository.getEncomendasByUser(req, res);
}

exports.encomenda_getByEstado = function (req, res) {
    repository.getEncomendasByEstado(req, res);
}


//GET ITENS DA ENCOMENDA BY ID
exports.encomenda_getItensDeEncomendaByID = function (req, res) {
    repository.getItensDeEncomendaByID(req, res);
};

//GET ITEM BY ID DA ENCOMENDA BY ID
exports.encomenda_getItemByIDDeEncomendaByID = function (req, res){
    repository.getItemByIDDeEncomendaByID(req, res);
}

//PUT
exports.encomenda_update = function (req, res) {
    repository.updateEncomenda(req, res);
};

exports.encomenda_updateComEstadoUser = function (req, res) {
    repository.updateEncomendaComEstadoUser(req, res);
};

exports.encomenda_updateUser = function (req, res) {
    repository.updateEncomendaUser(req, res);
};

//DELETE
exports.encomenda_delete = function (req, res) {
    repository.deleteEncomenda(req, res);
};