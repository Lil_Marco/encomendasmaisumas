const Fabrica = require('../models/Fabrica');
const repository = require('../repositories/FabricaRepository');

//POST
exports.post = function (req, res) {
    var fabrica = new Fabrica();

    repository.post(fabrica, req, res);
};

//GET ALL
exports.getAll = function (req, res) {
    repository.getAllFabricas(req, res);
}

//GET BY ID
exports.getFabricaByID = function (req, res) {
    repository.getFabricaByID(req, res);
};

exports.updateFabrica = function (req, res) {
    repository.updateFabrica(req, res);
};

exports.deleteFabrica = function (req, res) {
    repository.deleteFabrica(req, res);
}

exports.getCaminho = function (req, res) {
    repository.getCaminho(req, res);
};