const ItemDeProduto = require('../models/ItemDeProduto');
const Encomenda = require('../models/Encomenda');
const repository = require('../repositories/ItemDeProdutoRepository');
const Serviço = require('../services/Serviço');

var Client = require('node-rest-client').Client;
var client = new Client();

exports.post = function (req, res) {
    let itemDeProduto = new ItemDeProduto();

    var encomenda;
    Encomenda.findById(req.body.encomenda, function (err, objeto) {
        if (err) res.send(err);
        encomenda = objeto;
    })
    var preçoTotal = 0;

    const prom = new Promise((resolve, reject) => {

        var url = "https://meiditasfoifo.azurewebsites.net/api/Produto/" + req.body.produto;
        client.get(url, function (data, response) {
            if (!data) reject("error");
            else {
                itemDeProduto.produto = req.body.produto;

                console.log(data);
                Serviço.preencherItemPai(itemDeProduto, data);

                if (!Serviço.verificarMaterial(itemDeProduto, data, req.body.material)) {
                    res.json({ resultado: "Material do body não coincide com o Produto do catálogo!" });
                }
                const promAca = new Promise((resolveA, rejectA) => {

                    client.get('https://meiditasfoifo.azurewebsites.net/api/Material', function (data2, response) {

                        console.log("DATA:")
                        console.log(data2);

                        var acabamentos = [];

                        var nMateriais = data2.length;

                        for (var i = 0; i < nMateriais; i++) {

                            if (data2[i].nome == req.body.material) {

                                acabamentos = data2[i].acabamentos;
                            }
                        }

                        for (var i = 0; i < acabamentos.length; i++) {

                            if (acabamentos[i] == req.body.acabamento) {

                                itemDeProduto.acabamento = req.body.acabamento;
                                console.log(itemDeProduto.acabamento);
                            }

                        }

                        if (itemDeProduto.acabamento == null) {
                            res.json({ resultado: "Acabamento do body não coincidem com o Acabamento do Material!" });
                        }

                        resolveA(data2);
                    })
                });


                if (!Serviço.verificarDimensoes(itemDeProduto, data, req.body.altura, req.body.largura, req.body.profundidade)) {
                    res.json({ resultado: "Dimensões do body não coincidem com o Produto do catálogo!" });
                }

                preçoTotal = preçoTotal + itemDeProduto.preço;

                const allPromises = data.filhos.map(id => new Promise((resolve2, reject2) => {
                    url2 = "https://meiditasfoifo.azurewebsites.net/api/Produto/" + id;
                    client.get(url2, function (dataFilho, response2) {
                        if (!dataFilho) reject2("error2");
                        else {
                            let itemDeProdutoFilho = new ItemDeProduto();
                            itemDeProdutoFilho.produto = id;
                            if (!Serviço.verificarRestricoesMaterial(dataFilho.materiais, data.restricoesMaterial)) {
                                res.json({ resultado: "O filho não cumpre as restrições de material do pai!" });
                            }

                            if (!Serviço.verificarCaber(dataFilho.altura[0], dataFilho.largura[0],
                                dataFilho.profundidade[0], req.body.altura, req.body.largura, req.body.profundidade)) {
                                res.json({ resultado: "O filho não tem dimensões para caber no pai!" });
                            }

                            if (!Serviço.verificarRestricoesOcupacao(dataFilho.altura[0], dataFilho.largura[0],
                                dataFilho.profundidade[0], req.body.altura, req.body.largura, req.body.profundidade, data.restricaoOcup)) {
                                res.json({ resultado: "O filho não cumpre as restrições de ocupação do pai!" });
                            }

                            Serviço.preencherItemFilho(itemDeProdutoFilho, dataFilho);

                            repository.saveItemFilho(itemDeProdutoFilho);

                            itemDeProduto.itensFilho.push(itemDeProdutoFilho);
                            encomenda.itens.push(itemDeProdutoFilho.id);
                            preçoTotal = preçoTotal + itemDeProdutoFilho.preço;
                            resolve2(dataFilho);
                        }
                    })
                }));
                Promise.resolve(promAca)
                    .then(() => {
                       
                        Promise.all(allPromises)
                            .then(() => {
                                console.log("Os filhos foram criados");
                                resolve(data);
                            })
                            .catch((err) => {
                                console.log("Erro Filho");
                                return next(err);
                            });
                    })
                    .catch(function (err) {
                        console.log("Erro ACABAMENTOS");
                        return next(err);
                    });


            }
        });
    });

    Promise.resolve(prom)
        .then(() => {
            //encomenda.preço = encomenda.preço + preçoTotal;
            repository.saveItemPai(itemDeProduto, encomenda, preçoTotal, res);
        })
        .catch(function (err) {
            console.log(err);
            return (err);
        });


};

exports.item_create = function (req, res) {
    var item = new ItemDeProduto();

    repository.saveItem(item, req, res);
};

exports.item_create2 = function (req, res) {
    var item = new ItemDeProduto();
    var encomenda;
    const prom = new Promise((resolve, reject) => {
        
        Encomenda.findById(req.body.encomenda, function (err, objeto) {
            if (err) res.send(err);
            encomenda = objeto;
            resolve(objeto);
        })
    });
    
    Promise.resolve(prom)
        .then(() => {
            repository.saveItem2(item, encomenda, req, res);
            //repository.saveItem(item, req, res);
            //repository.saveItemPai(itemDeProduto, encomenda, preçoTotal, res);
        })
        .catch(function (err) {
            console.log(err);
            return (err);
        });  
}

exports.getAll = function (req, res) {
    repository.getAllItens(req, res);
}

//GET BY ID
exports.getItemDeProdutoByID = function (req, res) {
    repository.getItemDeProdutoByID(req, res);
};

//PUT
exports.updateItemDeProduto = function (req, res) {
    repository.updateItemDeProduto(req, res);
};

//DELETE
exports.deleteItemDeProduto = function (req, res) {
    repository.deleteItemDeProduto(req, res);
};