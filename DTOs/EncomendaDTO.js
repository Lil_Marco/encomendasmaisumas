class EncomendaDTO{

    encomendaDTO(encomenda){
        return {
            _id: encomenda._id,
            Identificador: encomenda.identificador,
            Cidade: encomenda.cidade,
            Preço: encomenda.preço,
            Data: encomenda.data,
            Estado: encomenda.estado,
            Itens: encomenda.itens,
            User: encomenda.user,
        }
    }

}

module.exports = new EncomendaDTO();