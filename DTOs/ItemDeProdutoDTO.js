class ItemDeProdutoDTO{

    ItemDeProdutoDTO(itemDeProduto){
        return {
            _id: itemDeProduto._id,
            Nome: itemDeProduto.nome,
            Preço: itemDeProduto.preço,
            Categoria: itemDeProduto.categoria,
            Material: itemDeProduto.material,
            Acabamento: itemDeProduto.acabamento,
            Dimensao: itemDeProduto.dimensao,
            ItensFilho: itemDeProduto.itensFilho,
            Produto: itemDeProduto.produto,
            Cor: itemDeProduto.cor
        }
    }

}

module.exports = new ItemDeProdutoDTO();